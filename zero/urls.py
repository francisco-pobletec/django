"""
URL configuration for zero project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from cliente import views
from cliente.views import ObrasDestacadasView, modificar_view, recuperar_clave_view
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', ObrasDestacadasView.as_view(), name='obras_destacadas'),
    path('obras-destacadas/', ObrasDestacadasView.as_view(), name='obras_destacadas'),
    path('pintura/', views.pintura_view, name='pintura'),
    path('escultura/', views.escultura_view, name='escultura'),
    path('orfebreria/', views.orfebreria_view, name='orfebreria'),
    path('tejido/', views.tejido_view, name='tejido'),
    path('otras/', views.otras_view, name='otras'),
    path('contacto/', views.contacto_view, name='contacto'),
    path('agregar/', views.agregar_view, name='agregar'),
    path('modificar/<id>/', modificar_view, name='modificar'),
    path('eliminar_view/<int:id>/', views.eliminar_view, name='eliminar_view'),
    path('login/', views.login_view, name='login'),
    path('registro/', views.registro_view, name='registro'),
    path('buscar/', views.buscar_view, name='buscar'),
    path('recuperar-clave/', recuperar_clave_view, name='recuperar_clave'),
    path('carrito/', views.carrito_view, name='carrito'),
    path('eliminar_del_carrito/<int:obra_id>/', views.eliminar_del_carrito, name='eliminar_del_carrito'),
    path('limpiar-carrito/', views.limpiar_carrito, name='limpiar_carrito'),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)