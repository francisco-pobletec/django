from django.urls import include, path
from .views import ObrasDestacadasView, modificar_view, recuperar_clave_view
from cliente import views

urlpatterns = [
    path('obras-destacadas/', ObrasDestacadasView.as_view(), name='obras_destacadas'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('pintura/', views.pintura_view, name='pintura'),
    path('escultura/', views.escultura_view, name='escultura'),
    path('orfebreria/', views.orfebreria_view, name='orfebreria'),
    path('tejido/', views.tejido_view, name='tejido'),
    path('otras/', views.otras_view, name='otras'),
    path('contacto/', views.contacto_view, name='contacto'),
    path('agregar/', views.agregar_view, name='agregar'),
    path('modificar/<id>/', modificar_view, name='modificar'),
    path('eliminar_view/<int:id>/', views.eliminar_view, name='eliminar_view'),
    path('login/', views.login_view, name='login'),
    path('registro/', views.registro_view, name='registro'),
    path('buscar/', views.buscar_view, name='buscar'),
    path('recuperar-clave/', recuperar_clave_view, name='recuperar_clave'),
    path('carrito/', views.carrito_view, name='carrito'),
    path('eliminar_del_carrito/<int:obra_id>/', views.eliminar_del_carrito, name='eliminar_del_carrito'),
    path('limpiar-carrito/', views.limpiar_carrito, name='limpiar_carrito'),
    
    
    # Agrega más URLs según sea necesario
]