
from django.shortcuts import get_object_or_404
from .models import Obra
from decimal import Decimal

class Carrito:
    def __init__(self, request):
        self.request = request
        self.session = request.session
        self.carrito = self.session.get("carrito", {})
        self.obras = []
    def get_obras_carrito(self):
        print("Llamada a la función get_obras_carrito")
        obra_ids = self.carrito.keys()
        obras = Obra.objects.filter(id__in=obra_ids)  # Utiliza "obra_ids" en lugar de "obra_id"
        carrito = self.carrito.copy()
        for obra in obras:
            obra_id = str(obra.id)
            carrito[obra_id]['obra'] = obra
            print(obra.titulo)
        obras_carrito = carrito.values()
        return obras_carrito
    def agregar(self, obra):
        print("Llamada a la función agregar")
        if hasattr(obra, 'id'):
            obra_id = str(obra.id)
            if obra_id not in self.carrito:
                self.carrito[obra_id] = {
                    "obra_id": obra_id,
                    "artista": obra.artista.nombre,  # Utiliza solo el nombre del artista
                    "titulo": obra.titulo,
                    "precio": str(obra.precio)  # Convierte el objeto Decimal a una cadena
                }
                self.obras.append(obra)
            else:
                self.carrito[obra_id]["precio"] = str(Decimal(self.carrito[obra_id]["precio"]) + obra.precio)  # Convierte a Decimal antes de sumar y luego convierte a cadena
            self.guardar_carrito()
        else:
            # Handle the case where the obra does not have an 'id' attribute
            # You can print an error message or take some specific action.
            pass

    def eliminar(self, obra):
        print("Llamada a la función eliminar")
        id = str(obra.id)
        if id in self.carrito:
            del self.carrito[id]
            self.obras.remove(obra)
            self.guardar_carrito()
    def restar(self, obra):
        print("Llamada a la función restar")
        id = str(obra.id)
        if id in self.carrito:
            self.carrito[id]["precio"] -= obra.precio
            if self.carrito[id]["precio"] <= 0:
                self.eliminar(obra)
            self.guardar_carrito()
    def guardar_carrito(self):
        print("Llamada a la función guardar_carrito")
        self.session["carrito"] = self.carrito
        self.session.modified = True
    def limpiar(self):
        print("Llamada a la función limpiar")
        self.carrito = {}
        self.obras = []
        self.guardar_carrito()
    def total_precio(request):
        if 'carrito' in request.session:
            carrito = request.session['carrito']
            total = sum(Decimal(obra_data['precio']) * obra_data['cantidad'] for obra_data in carrito.values())
        else:
            total = Decimal(0)

        return total

