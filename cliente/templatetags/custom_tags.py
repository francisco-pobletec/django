from django import template
from cliente.models import Obra

register = template.Library()

@register.simple_tag
def get_obra_by_id(obra_id):
    return Obra.objects.get(id=obra_id)
