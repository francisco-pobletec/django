def total_carrito(request):
    total = 0
    if request.user.is_authenticated:
        carrito = request.session.get("carrito", {})
        for key, value in carrito.items():
            if isinstance(value, dict) and "precio" in value:
                precio = value["precio"]
                if precio is not None:
                    total += int(precio)
    return {"total_carrito": total}


