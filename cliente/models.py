from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator

class Artista(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    def __str__(self):
        return f"{self.nombre} {self.apellido}"

class Obra(models.Model):
    artista = models.ForeignKey(Artista, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=100)
    descripcion = models.TextField()
    precio = models.DecimalField(max_digits=8, decimal_places=0)
    destacada = models.BooleanField(default=False)  # Campo 'destacada'
    imagen = models.ImageField(upload_to="obras", null=True)
    tipo_choices = [
        ('pintura', 'pintura'),
        ('orfebreria', 'orfebreria'),
        ('Otras', 'Otras'),
        ('escultura', 'escultura'),
        ('tejido', 'tejido'),
    ]
    tipo = models.CharField(max_length=10, choices=tipo_choices, default='Otras')

    def to_dict(self):
        return {
            'id': self.id,
            'artista': self.artista.to_dict(),
            'titulo': self.titulo,
            'descripcion': self.descripcion,
            'precio': str(self.precio),
            'destacada': self.destacada,
            'imagen': self.imagen.url if self.imagen else '',
            'tipo': self.tipo,
        }

    def __str__(self):
        return self.titulo


opciones_consultas = [
    (0, "consulta"),
    (1, "reclamo"),
    (2, "sugerencia"),
    (3, "felicitaciones"),
]


class Contacto(models.Model):
    nombre = models.CharField(max_length=50)
    correo = models.CharField(max_length=100)  # Agrega el atributo max_length aquí
    tipo_consulta = models.IntegerField(choices=opciones_consultas)
    mensaje = models.TextField(default='')
    avisos = models.BooleanField()

    def __str__(self):
        return self.nombre























class Cliente(models.Model):
    genero_choices = [
        ('M', 'Masculino'),
        ('F', 'Femenino'),
        ('O', 'Otro'),
    ]
    nombre = models.CharField(max_length=100, default='')
    apellido = models.CharField(max_length=100, default='')
    email = models.EmailField(default='')
    clave = models.CharField(max_length=100, default='')
    genero = models.CharField(max_length=1, choices=genero_choices)

    def __str__(self):
        return self.nombre + " " + self.apellido