
from django import forms
from django.contrib.auth.forms import UserCreationForm
from cliente.models import Artista, Contacto, Obra


class CustomUserCreationForm(UserCreationForm):
    pass

class ContactoForm(forms.ModelForm):
    class Meta:
        model = Contacto
        fields = ['nombre', 'correo', 'tipo_consulta', "mensaje",'avisos']

class ObraForm(forms.ModelForm):
    class Meta:
        model = Obra
        fields = '__all__'

class ArtistaForm(forms.ModelForm):
    class Meta:
        model = Artista
        fields = ['nombre', 'apellido']