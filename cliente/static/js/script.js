
function validateForm() {
    // Obtener los valores de los campos del formulario
    const nombre = document.getElementById("nombre").value;
    const apellido = document.getElementById("apellido").value;
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    const confirmPassword = document.getElementById("confirm-password").value;
    // Expresión regular para validar el nombre y apellido
    const nameRegex = /^[a-zA-Z]{3,20}$/;
    // Expresión regular para validar el correo electrónico
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    // Validar el nombre
    if (!nameRegex.test(nombre)) {
        console.log('nombre')
        alert("El nombre debe tener entre 3 y 20 letras");
        return false;
    }
    // Validar el apellido
    if (!nameRegex.test(apellido)) {
        alert("El apellido debe tener entre 3 y 20 letras");
        return false;
    }
    // Validar el correo electrónico
    if (!emailRegex.test(email)) {
        alert("El correo electrónico no es válido");
        return false;
    }
    // Validar la longitud de la clave
    if (password.length < 6) {
    alert("La clave debe tener al menos 6 caracteres.");
    return false;
}
    // Validar que las contraseñas sean iguales
    if (password !== confirmPassword) {
        alert("Las contraseñas no coinciden");
        return false;
    }
    // Si todas las validaciones pasan, retornar true para enviar el formulario
    return true;
}


function limpiarTablaCliente() {
    const tablaCliente = document.getElementById("tabla-cliente");
    while (tablaCliente.rows.length > 1) {
        tablaCliente.deleteRow(1);
    }
}

function limpiarTablaEmpleados() {
    const tablaEmpleados = document.getElementById("tabla-empleados");
    while (tablaEmpleados.rows.length > 1) {
        tablaEmpleados.deleteRow(1);
    }
}