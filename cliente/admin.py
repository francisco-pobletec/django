from django.contrib import admin
from .models import Artista, Obra, Contacto

admin.site.register(Artista)
class ArtistaAdmin(admin.ModelAdmin):
    list_display = ["nombre","apellido"]
    pass


class ObraAdmin(admin.ModelAdmin):
    list_display = ["id","titulo","precio"]
    list_editable =["precio"]
    search_fields = ["titulo","precio"]
    list_filter = []
    list_per_page = 5
    pass

admin.site.register(Obra,ObraAdmin)

admin.site.register(Contacto)