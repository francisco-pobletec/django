from decimal import Decimal
import re
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views import View
from .models import  Obra
from .forms import  ArtistaForm, ContactoForm,ObraForm
from django.core.paginator import Paginator
from django.db.models import Q
import logging
from .carrito import Carrito
from django.http import JsonResponse
class ObrasDestacadasView(View):
    def get(self, request):
        obras_destacadas = Obra.objects.filter(destacada=True)
        obras_sliced = [obras_destacadas[i:i+3] for i in range(0, len(obras_destacadas), 3)]
        data = {
            'obras': obras_sliced
        }
        return render(request, 'cliente/obras_destacadas.html', data)

    

def pintura_view(request):
    obras_pintura = Obra.objects.filter(tipo='pintura')
    data = {
        'obras': obras_pintura
    }
    return render(request, 'cliente/pintura.html', data)
def escultura_view(request):
    obras_escultura = Obra.objects.filter(tipo='escultura')
    data = {
        'obras': obras_escultura
    }
    return render(request, 'cliente/escultura.html', data)

def orfebreria_view(request):
    obras_orfebreria = Obra.objects.filter(tipo='orfebreria')
    data = {
        'obras': obras_orfebreria
    }
    return render(request, 'cliente/orfebreria.html', data)

def tejido_view(request):
    obras_tejido = Obra.objects.filter(tipo='tejido')
    data = {
        'obras': obras_tejido
    }
    return render(request, 'cliente/tejido.html', data,)

def otras_view(request):
    obras_otras = Obra.objects.filter(tipo='Otras')
    data = {
        'obras': obras_otras
    }
    return render(request, 'cliente/otras.html', data)

def contacto_view(request):
    data = {
        'form': ContactoForm()
    }
    if request.method == 'POST':
        formulario = ContactoForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "contacto guardado"
        else:
            data["form"] = formulario
    return render(request, 'cliente/contacto.html', data)
@login_required
def agregar_view(request):
    data = {
        'artista_form': ArtistaForm(),
        'obra_form': ObraForm()
    }
    if request.method == 'POST':
        if 'artista_form_submit' in request.POST:
            artista_formulario = ArtistaForm(request.POST)
            if artista_formulario.is_valid():
                artista_formulario.save()
                messages.success(request,"Artista guardado correctamente")
            else:
                data['artista_form'] = artista_formulario
        elif 'obra_form_submit' in request.POST:
            obra_formulario = ObraForm(request.POST, request.FILES)
            if obra_formulario.is_valid():
                obra_formulario.save()
                messages.success(request,"Obra guardada correctamente")
            else:
                data['obra_form'] = obra_formulario
    return render(request, 'cliente/agregar.html', data)

def listar_obras(request):
    Obras = Obra.objects.all()
    page = request.GET.get('page', 1)
    try:
        paginator = Paginator(Obras, 5)
        Obras = paginator.page(page)
    except:
        raise Http404
    data = {
        'obras': Obras,
        'paginator':paginator
    }
    return render(request, 'cliente/buscar.html', data)



def login_view(request):
    if request.method == 'GET':
        return render(request, 'login.html', {'form': AuthenticationForm()})
    else:
        name = request.POST["username"]
        password = request.POST["password"]
        if not name or not password:
            return render(request, 'login.html', {'form': AuthenticationForm(), 'error': 'Por favor, completa todos los campos.'})
        user = authenticate(username=name, password=password)
        if user is None:
            return render(request, 'login.html', {'form': AuthenticationForm(), 'error': 'Usuario y/o contraseña incorrectos.'})
        else:
            login(request, user)
            messages.success(request, '¡Inicio de sesión exitoso!')
            return redirect('obras_destacadas')




def registro_view(request):
    if request.method == 'GET':
        return render(request, 'registro.html', {'form': UserCreationForm()})
    else:
        password1 = request.POST["password1"]
        password2 = request.POST["password2"]
        if password1 != password2:
            return render(request, "registro.html", {'form': UserCreationForm(), 'error': "Las contraseñas no coinciden"})
        elif not (8 <= len(password1) <= 20):
            return render(request, "registro.html", {'form': UserCreationForm(), 'error': "La contraseña debe tener entre 8 y 20 caracteres"})
        else:
            name = request.POST["username"]
            if not re.match(r'^[\w\.-]+@[\w\.-]+\.\w+$', name):
                return render(request, "registro.html", {'form': UserCreationForm(), 'error': "El nombre de usuario debe ser una dirección de correo válida"})
            elif User.objects.filter(username=name).exists():
                return render(request, "registro.html", {'form': UserCreationForm(), 'error': "El nombre de usuario ya está en uso"})
            else:
                user = User.objects.create_user(username=name, password=password1)
                user.save()
                login(request, user)  # Iniciar sesión automáticamente después del registro
                messages.success(request, "¡Usuario registrado exitosamente!")
                return redirect('obras_destacadas')  # Redireccionar a las obras destacadas





def buscar_view(request):
    query = request.GET.get('query', '')
    obras = Obra.objects.filter(
        Q(artista__nombre__icontains=query) | Q(titulo__icontains=query)
    )
    
    if request.user.is_staff:
        paginator = Paginator(obras, 10)  # Cantidad de obras por página para el staff
    else:
        paginator = Paginator(obras, 9)  # Cantidad de obras por página para los clientes
    
    page_number = request.GET.get('page', 1)
    page_obj = paginator.get_page(page_number)
    
    data = {
        'obras': page_obj,
        'query': query,
    }
    return render(request, 'cliente/buscar.html', data)



def modificar_view(request, id):
    obra = get_object_or_404(Obra, id=id)
    if request.method == 'POST':
        formulario = ObraForm(data=request.POST, instance=obra, files=request.FILES)
        if formulario.is_valid():
            formulario.fields['artista'].disabled = True
            formulario.save()
            messages.success(request,"modificado correctamente")
            return redirect('buscar')
    else:
        formulario = ObraForm(instance=obra)
    # Agregar atributos id y name a cada campo del formulario
    for field_name, field in formulario.fields.items():
        field.widget.attrs['id'] = field_name
        field.widget.attrs['name'] = field_name
    data = {
        'form': formulario
    }
    return render(request, 'cliente/modificar.html', data)


def eliminar_view(request,id):
    obra = get_object_or_404(Obra, id=id)
    obra.delete()
    messages.success(request,"Eliminado correctamente")
    return redirect(to="buscar")

def salir(request):
    logout(request)
    return("obras_destacadas")

def recuperar_clave_view(request):
    if request.method == 'POST':
        form = PasswordResetForm(request.POST)
        messages.success(request, "Correo enviado. Verifica tu bandeja de entrada.")
        return redirect('login')  # Redirigir al usuario al inicio de sesión
    else:
        form = PasswordResetForm()
    
    return render(request, 'cliente/recuperar_clave.html', {'form': form})













def carrito_view(request):
    obra_id = request.GET.get('obra_id')

    if obra_id:
        try:
            obra = Obra.objects.get(id=obra_id)

            # Agregar la obra al carrito
            carrito = request.session.get('carrito', {})

            if obra_id in carrito:
                carrito[obra_id] += 1
            else:
                carrito[obra_id] = 1

            # Actualizar el carrito en la sesión
            request.session['carrito'] = carrito

        except Obra.DoesNotExist:
            return HttpResponse("La obra no existe")

    # Obtener el carrito de la sesión
    carrito = request.session.get('carrito', {})

    # Obtener los IDs de las obras en el carrito
    obra_ids = carrito.keys()

    # Obtener las obras correspondientes a los IDs del carrito
    obras = Obra.objects.filter(id__in=obra_ids)

    # Crear un diccionario para agrupar las obras por tipo
    obras_por_tipo = {}

    # Calcular el precio total del carrito
    total_precio = 0

    for obra in obras:
        tipo = obra.get_tipo_display()

        if tipo in obras_por_tipo:
            obras_por_tipo[tipo].append(obra)
        else:
            obras_por_tipo[tipo] = [obra]

        # Calcular el precio total sumando el precio de cada obra multiplicado por su cantidad en el carrito
        cantidad = carrito.get(str(obra.id), 0)
        total_precio += obra.precio * cantidad

    # Pasar los datos al contexto de la plantilla
    context = {
        'obras_por_tipo': obras_por_tipo,
        'total_precio': total_precio,
    }

    return render(request, 'cliente/carrito.html', context=context)










def total_carrito(request):
    total = 0
    if request.user.is_authenticated:
        carrito = request.session.get("carrito", {})
        for key, value in carrito.items():
            precio = value.get("precio")
            if precio is not None:
                total += int(precio)
    return {"carrito": total}

def eliminar_del_carrito(request, obra_id):
    carrito = request.session.get("carrito", {})
    if str(obra_id) in carrito:
        del carrito[str(obra_id)]
        request.session["carrito"] = carrito
        request.session.modified = True
    return redirect('carrito')

def limpiar_carrito(request):
    request.session.pop("carrito", None)
    request.session["carrito"] = {}
    request.session.modified = True
    return redirect('carrito')